		// UserInterface
		public function getRoles(){
			return array($this->getRole());
		}
		
		public function getSalt(){
			return null;
		}
		
		public function getUsername(){
			return $this->getNombre().' '.$this->getApellido1();
			//return $this->email;
		}
		
		public function eraseCredentials(){
			//TODO:
		}
		
		// Serializable
		public function serialize(){
			return serialize(array(
				$this->id,
				$this->email,
				$this->password
			));
		}
		
		public function unserialize($serialized){
			list(
				$this->id,
				$this->email,
				$this->password
			) = unserialize($serialized);
		}
		
		//EquatableInterface
		public function isEqualTo(UserInterface $user){
			return $this->id === $user->getId();
		}