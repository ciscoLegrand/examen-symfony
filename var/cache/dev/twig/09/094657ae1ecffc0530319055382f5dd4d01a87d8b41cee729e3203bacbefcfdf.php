<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Profesor/menu.html.twig */
class __TwigTemplate_24c5d1a27e23d6598b202e5bee8fea73c7a93ff0f82d5fa0a5e61ed39b0545a9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Profesor/menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Profesor/menu.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "Profesor/menu.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Profesor";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<p>Buenos días ";
        // line 8
        echo twig_escape_filter($this->env, ($context["username"] ?? $this->getContext($context, "username")), "html", null, true);
        echo ",</p>


<div style=\"\">
\t<form action=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lista_alumnos_profesor");
        echo "\" method=\"post\">
\t\t<input type=\"submit\" value=\"lista Alumnos\"/>
\t</form>
\t<form action=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lista_alumnos_json");
        echo "\" method=\"post\">
\t\t<input type=\"submit\" value=\"lista json\"/>
\t</form>
\t<h3>Generar un examen</h3>
\t<form action=\"/profesor/genera_examen\" method=\"get\">
\t\t<input type=\"submit\" value=\"teorico\"/>
\t<input type=\"submit\" value=\"practico\"/>
\t</form>
<br>
\t<form action=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
        echo "\" method=\"post\">
\t\t<input type=\"submit\" value=\"logout\"/>
\t</form>



\t\t\t<!--<div id=\"panelIncluirAlumno\">
\t\t\t<button id=\"botonNuevoAlumno\" onclick=\"incluirNuevoAlumno_onClick()\">Nuevo alumno</button>
\t\t</div>-->
\t\t";
        // line 33
        if ((isset($context["lista"]) || array_key_exists("lista", $context))) {
            // line 34
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["lista"] ?? $this->getContext($context, "lista")));
            foreach ($context['_seq'] as $context["_key"] => $context["alumno"]) {
                // line 35
                echo "\t\t\t<li>";
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($context["alumno"], "nombre", [])), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($context["alumno"], "apellido1", [])), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($context["alumno"], "apellido2", [])), "html", null, true);
                echo "</li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['alumno'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "\t\t";
        }
        // line 38
        echo "
";
        // line 39
        if ((isset($context["name"]) || array_key_exists("name", $context))) {
            // line 40
            echo "
";
        }
        // line 42
        echo "


\t</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 50
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 51
        echo "
<script>

function incluirNuevoAlumno_onClick(){
\tmy_form=document.createElement('FORM');
\tmy_form.name='myForm';
\tmy_form.method='GET';
\tmy_form.action='index.php';

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='TEXT';
\tmy_tb.name='nombre';
\tmy_tb.placeholder=\"Nombre\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='TEXT';
\tmy_tb.name='apellido';
\tmy_tb.placeholder=\"Apellido\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='hidden';
\tmy_tb.name='accion';
\tmy_tb.value=\"2\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='submit';
\tmy_tb.value='Incluir nuevo alumno al Sistema';
\tmy_form.appendChild(my_tb);

\tdocument.getElementById(\"panelIncluirAlumno\").appendChild(my_form);
\tdocument.getElementById(\"botonNuevoAlumno\").remove();
}

function simularExamen_onClick(){
\tbuttonTeorico=document.createElement('BUTTON');
\tbuttonTeorico.innerHTML = \"Simula Examen Teórico\";
\tbuttonTeorico.onclick=function(){
\t\tpost(\"index.php\", {accion: \"3\", examen:\"teorico\"}, \"get\")
\t};
\tbuttonPractico=document.createElement('BUTTON');
\tbuttonPractico.innerHTML = \"Simula Examen Práctico\";
\tbuttonPractico.onclick=function(){
\t\tpost(\"index.php\", {accion: \"3\", examen:\"practico\"}, \"get\")
\t};

\tdocument.getElementById(\"panelSimularExamen\").appendChild(buttonTeorico);
\tdocument.getElementById(\"panelSimularExamen\").appendChild(buttonPractico);
\tdocument.getElementById(\"botonSimulaExamen\").remove();
}

function imprimirListaAlumnos_onClick(){
\tpost(\"index.php\", {accion: \"1\"}, \"get\")
}

function logout_onClick(){
\tpost(\"index.php\", {accion: \"5\"}, \"get\")
}

/**
* sends a request to the specified url from a form. this will change the window location.
* @param {string} path the path to send the post request to
* @param {object} params the paramiters to add to the url
* @param {string} [method=post] the method to use on the form
*/

function post(path, params, method) {
\tmethod = method || \"post\"; // Set method to post by default if not specified.

\t// The rest of this code assumes you are not using a library.
\t// It can be made less wordy if you use one.
\tvar form = document.createElement(\"form\");
\tform.setAttribute(\"method\", method);
\tform.setAttribute(\"action\", path);

\tfor(var key in params) {
\t\tif(params.hasOwnProperty(key)) {
\t\t\tvar hiddenField = document.createElement(\"input\");
\t\t\thiddenField.setAttribute(\"type\", \"hidden\");
\t\t\thiddenField.setAttribute(\"name\", key);
\t\t\thiddenField.setAttribute(\"value\", params[key]);

\t\t\tform.appendChild(hiddenField);
\t\t}
\t}

\tdocument.body.appendChild(form);
\tform.submit();
}

</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "Profesor/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 51,  167 => 50,  152 => 42,  148 => 40,  146 => 39,  143 => 38,  140 => 37,  127 => 35,  122 => 34,  120 => 33,  108 => 24,  96 => 15,  90 => 12,  83 => 8,  80 => 7,  71 => 6,  53 => 4,  31 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
{% extends 'base.html.twig' %}

{% block title %}Menu Profesor{% endblock %}

{% block body %}

<p>Buenos días {{ username }},</p>


<div style=\"\">
\t<form action=\"{{ path(\"lista_alumnos_profesor\") }}\" method=\"post\">
\t\t<input type=\"submit\" value=\"lista Alumnos\"/>
\t</form>
\t<form action=\"{{ path(\"lista_alumnos_json\") }}\" method=\"post\">
\t\t<input type=\"submit\" value=\"lista json\"/>
\t</form>
\t<h3>Generar un examen</h3>
\t<form action=\"/profesor/genera_examen\" method=\"get\">
\t\t<input type=\"submit\" value=\"teorico\"/>
\t<input type=\"submit\" value=\"practico\"/>
\t</form>
<br>
\t<form action=\"{{ path(\"logout\") }}\" method=\"post\">
\t\t<input type=\"submit\" value=\"logout\"/>
\t</form>



\t\t\t<!--<div id=\"panelIncluirAlumno\">
\t\t\t<button id=\"botonNuevoAlumno\" onclick=\"incluirNuevoAlumno_onClick()\">Nuevo alumno</button>
\t\t</div>-->
\t\t{% if lista is defined %}
\t\t{% for alumno in lista %}
\t\t\t<li>{{ alumno.nombre | upper }} {{ alumno.apellido1 | upper }} {{ alumno.apellido2 | upper }}</li>
\t\t{% endfor %}
\t\t{% endif %}

{% if name is defined %}

{% endif %}



\t</div>

{% endblock %}


{% block javascripts %}

<script>

function incluirNuevoAlumno_onClick(){
\tmy_form=document.createElement('FORM');
\tmy_form.name='myForm';
\tmy_form.method='GET';
\tmy_form.action='index.php';

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='TEXT';
\tmy_tb.name='nombre';
\tmy_tb.placeholder=\"Nombre\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='TEXT';
\tmy_tb.name='apellido';
\tmy_tb.placeholder=\"Apellido\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='hidden';
\tmy_tb.name='accion';
\tmy_tb.value=\"2\";
\tmy_form.appendChild(my_tb);

\tmy_tb=document.createElement('INPUT');
\tmy_tb.type='submit';
\tmy_tb.value='Incluir nuevo alumno al Sistema';
\tmy_form.appendChild(my_tb);

\tdocument.getElementById(\"panelIncluirAlumno\").appendChild(my_form);
\tdocument.getElementById(\"botonNuevoAlumno\").remove();
}

function simularExamen_onClick(){
\tbuttonTeorico=document.createElement('BUTTON');
\tbuttonTeorico.innerHTML = \"Simula Examen Teórico\";
\tbuttonTeorico.onclick=function(){
\t\tpost(\"index.php\", {accion: \"3\", examen:\"teorico\"}, \"get\")
\t};
\tbuttonPractico=document.createElement('BUTTON');
\tbuttonPractico.innerHTML = \"Simula Examen Práctico\";
\tbuttonPractico.onclick=function(){
\t\tpost(\"index.php\", {accion: \"3\", examen:\"practico\"}, \"get\")
\t};

\tdocument.getElementById(\"panelSimularExamen\").appendChild(buttonTeorico);
\tdocument.getElementById(\"panelSimularExamen\").appendChild(buttonPractico);
\tdocument.getElementById(\"botonSimulaExamen\").remove();
}

function imprimirListaAlumnos_onClick(){
\tpost(\"index.php\", {accion: \"1\"}, \"get\")
}

function logout_onClick(){
\tpost(\"index.php\", {accion: \"5\"}, \"get\")
}

/**
* sends a request to the specified url from a form. this will change the window location.
* @param {string} path the path to send the post request to
* @param {object} params the paramiters to add to the url
* @param {string} [method=post] the method to use on the form
*/

function post(path, params, method) {
\tmethod = method || \"post\"; // Set method to post by default if not specified.

\t// The rest of this code assumes you are not using a library.
\t// It can be made less wordy if you use one.
\tvar form = document.createElement(\"form\");
\tform.setAttribute(\"method\", method);
\tform.setAttribute(\"action\", path);

\tfor(var key in params) {
\t\tif(params.hasOwnProperty(key)) {
\t\t\tvar hiddenField = document.createElement(\"input\");
\t\t\thiddenField.setAttribute(\"type\", \"hidden\");
\t\t\thiddenField.setAttribute(\"name\", key);
\t\t\thiddenField.setAttribute(\"value\", params[key]);

\t\t\tform.appendChild(hiddenField);
\t\t}
\t}

\tdocument.body.appendChild(form);
\tform.submit();
}

</script>

{% endblock %}
", "Profesor/menu.html.twig", "/home/cisco/Escritorio/dwcs/examen3a/edu-php-symfony-problema-alumnos-y-profesores/app/Resources/views/Profesor/menu.html.twig");
    }
}
