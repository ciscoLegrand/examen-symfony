<?php namespace AppBundle\Model;

/**
 * Representa todas las consultas que se puedan hacer a la parte administrativa del centro
 * @authors Enrique Agrasar
 */
interface Oficina{
	
	/** Retorna todas los alumnos matriculados en una Materia */
	public function getAlumnosFromMateria(Materia $pMateria):?array;
	
}