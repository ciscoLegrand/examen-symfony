<?php namespace AppBundle\Model;

use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
*	La clase Alumno va a representar a un usuario del sistema

*	@ORM\Table(name="alumnos")
*	@ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
*/
class Alumno implements UserInterface, Serializable{
	
	// Atributos
	
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	private $dni;
	private $email;
	private $nombre;
	private $apellido1;
	private $apellido2;
	
	private $_misExamenes = array();
	private $_misMatriculas = array();
	
	// Getters y Setters
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}
	
	// Construccion
	public function __construct(string $pDni, string $pNombre, string $pApellido1, string $pApellido2) {
		$this->dni = $pDni;
		$this->nombre = $pNombre;
		$this->apellido1 = $pApellido1;
		$this->apellido2 = $pApellido2;
	}
	
	// Métodos de interface
	public function entregaExamen(Examen $pExamen) {
		array_push($this->_misExamenes,$pExamen);
	}

	// Métodos de interface
	public function dameTusExamenes() {
		return $this->_misExamenes;
	}
	
	public function dameTusExamenesDeMateria(Materia $pMateria){
		$examenes = array();
		foreach($this->_misExamenes as $unExamen){
			if($unExamen->esDeMateria($pMateria)){
				array_push($examenes, $unExamen);
			}
		}
		return $examenes;
	}

	public function dameMediaDeTusExamenes() {
		$acumulado = 0;
		$numExamenes = 0;
		foreach ($this->dameTusExamenes() as $unExamen) {
			$acumulado += $unExamen->_nota;
			$numExamenes++;
		}
		if ($numExamenes > 0)
			return $acumulado / $numExamenes;
		else
			return (float) 0;
	}
	
	public function dameMediaDeTusExamenesDeMateria(Materia $pMateria) {
		$acumulado = 0;
		$numExamenes = 0;
		foreach ($this->dameTusExamenesDeMateria($pMateria) as $unExamen) {
			$acumulado += $unExamen->_nota;
			$numExamenes++;
		}
		if ($numExamenes > 0)
			return $acumulado / $numExamenes;
		else
			return (float) 0;
	}
	
	public function matricularEnMateria(Materia $pMateria) {
		array_push($this->_misMatriculas, $pMateria);
	}
	
	public function getMatriculas(){
		$matriculas = $this->_misMatriculas;
		return $matriculas;
	}
	
	public function estasMatriculadoEn(Materia $pMateria){
		foreach($this->_misMatriculas as $unaMateria){
			if($unaMateria == $pMateria){
				return true;
			}
		}
		return false;
	}
	
	
	// UserInterface
	public function getRoles(){
		//TODO:
	}
	
	public function getPassword(){
		//TODO:
	}
	
	public function getSalt(){
		//TODO:
	}
	
	public function getUsername(){
		//TODO:
	}
	
	public function eraseCredentials(){
		//TODO:
	}
	
	// Serializable
	public function serialize(){
		return serialize(array(
			$this->id,
			$this->email,
			$this->password
		));
	}
	
	public function unserialize($serialized){
		list(
			$this->id,
			$this->email,
			$this->password
		) = unserialize($serialized);
	}
	
}