<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AlumnoController extends Controller
{
    /**
     * @Route("/alumno",name="menu_alumno")
     */
    public function indexAction()
    {
      $user = $this->getUser();
      return $this->render('Alumno/show.html.twig', array(
          "username" => $user->getUsername(),#accedemos a la funcion de la entidad User que nos da nombre y apellido
      ));
    }

}
