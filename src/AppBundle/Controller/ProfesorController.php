<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Profesor;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProfesorController extends Controller
{
  /**
  * @Route("/profesor", name="menu_profesor")
  * @Security("has_role('ROLE_PROF')")
  */
  public function indexAction()
  {
    $user = $this->getUser();
    return $this->render('Profesor/menu.html.twig', array(
      "username" => $user->getUsername()
    ));
  }

  /**
  * @Route("/profesor/lista", name="lista_alumnos_profesor")
  * @Security("has_role('ROLE_PROF')")
  */
  public function listarAlumnosAction(){

    $user = $this->getUser();
    return $this->render('Profesor/menu.html.twig', array(
      "username" => $user->getUsername(),
    "lista" => Profesor::getListaAlumnos()

    ));
  }

  /**
  * @Route("/profesor/listaAlumnos", name="lista_alumnos_json")
  * @Security("has_role('ROLE_PROF')")
  */
  public function listarAlumnosJsonAction(){
    $profe= new Profesor;

    $data = [
      'alumnos' => $profe->getListaAlumnos()
    ];
    return new JsonResponse($data);
  }

  /**
  * @Route("/profesor/genera_examen/{name}", name="genera_examen")
  * @Security("has_role('ROLE_PROF')")
  */
  public function generaExamenAction($name){
    $user=$this->getUser();

      return $this->render('Profesor/teorico.html.twig', array(
        "username" => $user->getUsername(),
        "name"=>$name,
        'teorico'=>'has generado un examen teorico',
        'practico'=>'has generado un examen practico'
        ));


    

  }
}
